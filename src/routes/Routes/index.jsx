import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "../../pages/Home";
import Login from "../../pages/Login";
import { Add } from "../../components/Add";

import { Watchlist } from "../../components/Watchlist";
import { Watched } from "../../components/Watched";
import { TopSeries } from "../../components/TopSeries";
import { TopFilms } from "../../components/TopFilms";

import Registration from "../../pages/Registration";
import Profile from "../../pages/Profile";
import NotFound from "../../pages/NotFound";
import useAuth from "../../hooks/useAuth";
import PrivateRoute from "../components/PrivateRoute";
import GuestRoute from "../components/GuestRoute";
import { GlobalProvider } from "../../GlobalContext/GlobalState";
import { NAVIGATION_URLS } from "../../utils/navigationUrls";

import {
  CircularProgress,
  makeStyles,
  Container,
  Grid,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
}));

function Routes() {
  const classes = useStyles();
  const auth = useAuth();

  return auth.isLoaded ? (
    <GlobalProvider>
      <Switch>
        {NAVIGATION_URLS.map((navigation) => {
          return navigation.isPrivate ? (
            <PrivateRoute key={navigation.name} exact path={navigation.path}>
              {React.createElement(navigation.component, {})}
            </PrivateRoute>
          ) : (
            // <PrivateRoute
            //   exact
            //   key={navigation.name}
            //   path={navigation.path}
            //   component={navigation.component}
            // />
            <Route
              exact
              key={navigation.name}
              path={navigation.path}
              component={navigation.component}
            />
          );
        })}

        {/* <Route exact path="/">
          <Home />
        </Route> */}
        {/* <Route exact path="/topseries">
          <TopSeries />
        </Route>
        <Route exact path="/topfilms">
          <TopFilms />
        </Route> */}
        <PrivateRoute path="/profile">
          <Profile />
        </PrivateRoute>

        <GuestRoute path="/login">
          <Login />
        </GuestRoute>
        {/* <Route path="/add">
          <Add />
        </Route> */}
        {/* <PrivateRoute path="/watchlist">
          <Watchlist />
        </PrivateRoute> */}
        {/* <PrivateRoute path="/watched">
          <Watched />
        </PrivateRoute> */}
        <GuestRoute path="/registration">
          <Registration />
        </GuestRoute>

        <Route path="/not-found-404">
          <NotFound />
        </Route>
        <Redirect to="/not-found-404" />
      </Switch>
    </GlobalProvider>
  ) : (
    <Container maxWidth="md" className={classes.root}>
      <Grid container spacing={3} alignItems="center" justify="center">
        <Grid item>
          <CircularProgress color="inherit" />
        </Grid>
      </Grid>
    </Container>
  );
}

export default Routes;
