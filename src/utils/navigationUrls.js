import Home from "../pages/Home/index";
import { Add } from "../components/Add";
import { Watchlist } from "../components/Watchlist";
import { Watched } from "../components/Watched";
import { TopFilms } from "../components/TopFilms";
import { TopSeries } from "../components/TopSeries";

export const NAVIGATION_URLS = [
  { name: "Home", path: "/", component: Home, isPrivate: false },

  { name: "Add", path: "/add", component: Add, isPrivate: true },

  {
    name: "TopFilms",
    path: "/topfilms",
    component: TopFilms,
    isPrivate: false,
  },
  {
    name: "TopSeries",
    path: "/topseries",
    component: TopSeries,
    isPrivate: false,
  },
  {
    name: "Watchlist",
    path: "/watchlist",
    component: Watchlist,
    isPrivate: true,
  },
  {
    name: "Watched",
    path: "/watched",
    component: Watched,
    isPrivate: true,
  },
];
//   : "",
//   Watchlist: "watchlist",
//   Watched: "watched",
//   TopSeries: "topseries",
//   TopFilms: "topfilms",
