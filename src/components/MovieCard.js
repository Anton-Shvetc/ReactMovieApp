import React from "react";

import { MovieControls } from "../components/MovieControls";
import { GlobalProvider } from "../GlobalContext/GlobalState";

export const MovieCard = ({ movie, type }) => {
  return (
    <div className="movie-card">
      <div className="overlay"> </div>
      {movie.poster_path ? (
        <img
          src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`}
          alt={`${movie.title} Постер`}
        ></img>
      ) : (
        <div className="filler-poster"></div>
      )}
{/* <GlobalProvider> */}
      <MovieControls type={type} movie={movie} />
      {/* </GlobalProvider> */}
    </div>
  );
};
