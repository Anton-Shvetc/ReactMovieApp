import React, { useState } from "react";
import { GlobalContext } from "../GlobalContext/GlobalState";
import { MovieCard } from "./MovieCard";
import { ResultCard } from "./ResultCard";
import { MovieControls } from "./MovieControls";

export const TopFilms = () => {
  const [items, setResults] = useState([]);

  // const tmdb_url = "https://www.themoviedb.org/movie";
  const api_url = "https://api.themoviedb.org";
  // const image_url = "https://image.tmdb.org/t/p/w500";
  const api_key = "b03ab547ea41fd0143dde1d1a019143e";
  const language = "ru";
  const type = "movie";
  const genres = {
    Action: 28,
    Adventure: 12,
    Animation: 16,
    Comedy: 35,
    Crime: 80,
    Document: 99,
    Drama: 18,
    Family: 10751,
    Fantasy: 14,
    History: 36,
    Horror: 27,
    Music: 10402,
    Mystery: 9648,
    Romance: 10749,
    SciFi: 878,
    TVMovie: 10770,
    Thriller: 53,
    War: 10752,
    Western: 37,
  };
  const API_KEY = "b03ab547ea41fd0143dde1d1a019143e";
  const BASE_URL = "https://api.themoviedb.org/3";
  const API_URL =
    BASE_URL + "/discover/movie?sort_by=popularity.desc" + API_KEY;

  const fetchData = () => {
    fetch(
      `https://api.themoviedb.org/3/discover/movie?with_genres=35&with_cast=23659&sort_by=revenue.desc&api_key=${api_key}`
    ) // `${api_url}/3/discover/${type}?&api_key=${api_key}&language=${language}}`
      .then((res) => res.json())
      .then((data) => {
        if (!data.errors) {
          // console.log(data);
          setResults(data.results);

          // setResults(console.log(data.results));
        } else {
        }
      });
  };
  fetchData();
  return (
    <div className="movie-page">
      <div className="container">
        <div className="header">
          <h1 className="heading">Фильмы</h1>
          {/* <h1 className="heading">{`Top ${genres[3].name}`}</h1> */}
        </div>
        <div>
          {/* <button className="fetch-button" onClick={fetchData}>
            Показать фильмы
          </button> */}
        </div>
        <div className="movie-grid">
          {items &&
            items.map((items, index) => {
              // const cleanedDate = new Date(items.released).toDateString();
              // const authors = book.authors.join(", ");

              return (
                <div className="movie-card" key={index}>
                  <h3 className="movie-card_title">{items.title}</h3>
                  <img
                    src={`https://image.tmdb.org/t/p/w500${items.poster_path}`}
                    alt={`${items.title} Постер`}
                  ></img>
                  <p className="movie-card_average">{`Рейтинг: ${items.vote_average}`}</p>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

// export const TopFilms = () => {
//   const [books, setBooks] = useState(null);

//   const fetchData = () => {
//     fetch(`https://www.anapioficeandfire.com/api/books?pageSize=30`)
//       //https://api.themoviedb.org/3/movie?api_key=b03ab547ea41fd0143dde1d1a019143e&query=harry potter
//       .then((res) => res.json())
//       .then((data) => {
//         if (!data.errors) {
//           setBooks(data);
//         } else {
//           // setResults([]);
//         }
//       });
//   };

//   return (
//     <div className="movie-page">
//       <div className="container">
//         <div className="header">
//           <h1>Game of Thrones Books</h1>
//           <h2>Fetch a list from an API and display it</h2>

//           <div>
//             <button className="fetch-button" onClick={fetchData}>
//               Fetch Data
//             </button>
//           </div>

//           <div className="books">
//             {books &&
//               books.map((book, index) => {
//                 const cleanedDate = new Date(book.released).toDateString();
//                 const authors = book.authors.join(", ");

//                 return (
//                   <div className="book" key={index}>
//                     <h3>Book {index + 1}</h3>
//                     <h2>{book.name}</h2>

//                     <div className="details">
//                       <p>{authors}</p>
//                       <p>{book.numberOfPages} pages</p>
//                       <p>🏘{book.country}</p>
//                       <p>{cleanedDate}</p>
//                     </div>
//                   </div>
//                 );
//               })}
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };
