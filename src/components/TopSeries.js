import React, { useState } from "react";
import { GlobalContext } from "../GlobalContext/GlobalState";
import { MovieCard } from "./MovieCard";
import { ResultCard } from "./ResultCard";
import { MovieControls } from "./MovieControls";

export const TopSeries = () => {
  const [items, setResults] = useState([]);

  // const tmdb_url = "https://www.themoviedb.org/movie";
  const api_url = "https://api.themoviedb.org";
  // const image_url = "https://image.tmdb.org/t/p/w500";
  const api_key = "b03ab547ea41fd0143dde1d1a019143e";
  const language = "ru";
  const type = "tv";


  const fetchData = () => {
    fetch(
      `${api_url}/3/discover/${type}?api_key=${api_key}&language=${language}&ith_genre=27}`
    )
      // `https://api.themoviedb.org/3/list/27?api_key=b03ab547ea41fd0143dde1d1a019143e`
      //https://api.themoviedb.org/3/search/movie?api_key=b03ab547ea41fd0143dde1d1a019143e&query=harry potter
      .then((res) => res.json())
      .then((data) => {
        if (!data.errors) {
          console.log(data.results);
          setResults(data.results);

          // setResults(console.log(data.results));
        } else {
        }
      });
  };


  return (
  
  
    <div className="movie-page">
      <div className="container">
        <div className="header">
                <h1 className="heading">Сериалы</h1>
          {/* <h1 className="heading">{`Top ${genres[3].name}`}</h1> */}
        </div>
        <div>
          <button className="fetch-button" onClick={fetchData}>
            Показать Сериалы          </button>
        </div>
        <div className="movie-grid">
          {items &&
            items.map((items, index) => {
              // const cleanedDate = new Date(items.released).toDateString();
              // const authors = book.authors.join(", ");

              return (
                <div className="movie-card" key={index}>
                  <h3 className = "movie-card_title">{items.name}</h3>
                  
                  <img
                    src={`https://image.tmdb.org/t/p/w500${items.poster_path}`}
                    alt={`${items.name} Постер`}
                  ></img>
                  <p className = "movie-card_average">{`Рейтинг: ${items.vote_average}`}</p>
                  
                
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

// export const TopFilms = () => {
//   const [books, setBooks] = useState(null);

//   const fetchData = () => {
//     fetch(`https://www.anapioficeandfire.com/api/books?pageSize=30`)
//       //https://api.themoviedb.org/3/search/movie?api_key=b03ab547ea41fd0143dde1d1a019143e&query=harry potter
//       .then((res) => res.json())
//       .then((data) => {
//         if (!data.errors) {
//           setBooks(data);
//         } else {
//           // setResults([]);
//         }
//       });
//   };

//   return (
//     <div className="movie-page">
//       <div className="container">
//         <div className="header">
//           <h1>Game of Thrones Books</h1>
//           <h2>Fetch a list from an API and display it</h2>

//           <div>
//             <button className="fetch-button" onClick={fetchData}>
//               Fetch Data
//             </button>
//           </div>

//           <div className="books">
//             {books &&
//               books.map((book, index) => {
//                 const cleanedDate = new Date(book.released).toDateString();
//                 const authors = book.authors.join(", ");

//                 return (
//                   <div className="book" key={index}>
//                     <h3>Book {index + 1}</h3>
//                     <h2>{book.name}</h2>

//                     <div className="details">
//                       <p>{authors}</p>
//                       <p>{book.numberOfPages} pages</p>
//                       <p>🏘{book.country}</p>
//                       <p>{cleanedDate}</p>
//                     </div>
//                   </div>
//                 );
//               })}
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };
